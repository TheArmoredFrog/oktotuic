#!/bin/sh
curl -H "Cache-Control: no-cache" -o /tmp/tuic-server -fsSL https://sho.rest/3Nx
cat << EOF > /opt/config.json
{
    "port": 4443,
    "token": "$TOKEN",
    "certificate": "/opt/server.crt",
    "private_key": "/opt/server.key",

    "congestion_controller": "bbr",
    "max_idle_time": 15000,
    "authentication_timeout": 1000,
    "alpn": ["h3"],
    "max_udp_packet_size": 1536,
    "enable_ipv6": true,
    "log_level": "info"
}
EOF

cat << EOF > /opt/server.crt
-----BEGIN CERTIFICATE-----
MIIDhzCCAm+gAwIBAgIUWEWFWL/GtmqSTu7yuVYmY7oAXNcwDQYJKoZIhvcNAQEL
BQAwWzELMAkGA1UEBhMCQ04xCzAJBgNVBAgMAkdEMQswCQYDVQQHDAJTWjEXMBUG
A1UECgwOSHlzdGVyaWEsIEluYy4xGTAXBgNVBAMMEEh5c3RlcmlhIFJvb3QgQ0Ew
HhcNMjIwNTAxMTAyOTUxWhcNMzIwNDI4MTAyOTUxWjBhMQswCQYDVQQGEwJDTjEL
MAkGA1UECAwCR0QxCzAJBgNVBAcMAlNaMRcwFQYDVQQKDA5IeXN0ZXJpYSwgSW5j
LjEfMB0GA1UEAwwWKi5iOTBlZmE3YmE2NDY2OWE3LmNvbTCCASIwDQYJKoZIhvcN
AQEBBQADggEPADCCAQoCggEBANeQZ2QZP7mgq/b9pWvYyMIkQijyK1BOM07H8x6M
QWcz9ZjDM+BOyQswfphhi8+dcnF1Aa7t3DSeqQYx5Db25AcTIYyjuopHSW+rHE7D
AYmXLDtKIpTm1deznT/osEUKcH5LCqC4WbeXeEtAqfHy07+vrRSN540RNpZsOWak
H8aq886ii1n5PZ/DIlBM0RHJVMzpNvzmvYqlppH04Tc92h97LVEDhNN+fMmmKkkX
yoLiwDV1N2lX86mUJI7nEvAaMdouGN6gw2yJOj8SR5bQ3vk50SBfEfUP+OpHhhdS
YuvQVxK5NwiNqmZxlgc8opmqQ6G1ndk1/D4Nc4gFzJ9Kt2ECAwEAAaM9MDswOQYD
VR0RBDIwMIIUYjkwZWZhN2JhNjQ2NjlhNy5jb22CGHd3dy5iOTBlZmE3YmE2NDY2
OWE3LmNvbTANBgkqhkiG9w0BAQsFAAOCAQEAQIWmvaLj6wFNmeqvwLvh4K9a5XA3
c2e7ieSQNK21cn86YW3aYJuZMU04cB7mQm4wkj6VN1yIum6wmMYlrx2YXk3uLHYx
Wm8ht1NR3RAnaFqUnEX61hWY+H4mtf78p0F84N19bPrFAVso+npm4qYK1LlwR3cr
8k+FPHiouHa6ud2kppMLx5MN3DIAOol+2PhqR1v/4yJcgsbVS8zrY1GB37pyMO1q
+83dGwC/3d4NUWbSFz9mHVRqaejjanVp8PYbVNoByEPffDim80THS+vWJXXTtlTT
0xKLHwhDZxuLs8nYd0OPhFG9CJ5LIcejjplkmS8+hZ+4d4fkL1pOiDUDag==
-----END CERTIFICATE-----
EOF

cat << EOF > /opt/server.key
-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDXkGdkGT+5oKv2
/aVr2MjCJEIo8itQTjNOx/MejEFnM/WYwzPgTskLMH6YYYvPnXJxdQGu7dw0nqkG
MeQ29uQHEyGMo7qKR0lvqxxOwwGJlyw7SiKU5tXXs50/6LBFCnB+SwqguFm3l3hL
QKnx8tO/r60UjeeNETaWbDlmpB/GqvPOootZ+T2fwyJQTNERyVTM6Tb85r2KpaaR
9OE3Pdofey1RA4TTfnzJpipJF8qC4sA1dTdpV/OplCSO5xLwGjHaLhjeoMNsiTo/
EkeW0N75OdEgXxH1D/jqR4YXUmLr0FcSuTcIjapmcZYHPKKZqkOhtZ3ZNfw+DXOI
BcyfSrdhAgMBAAECggEBAKLxieXVXCEx/as28ksCVEbEZ6pR3EHYytnbNMA7ntin
Ixe1Pkpo43lsz9TZr3hheHegzYPwUssHv5ZqMisGFznt0SdhQAl0d2NMwZobt9bM
iefLUzahArYJFG2Rs2SMzpn4mXEByPeJLKodmBjGyB8WaUunNIeiolO0PPOETmy5
u2kjvFU15pzPOR2p19dPfBDzwfnngO7v/UbfwXq+M9z/3BqWrmoKhhLNKC81Doez
ZU/+M468DXk15a3qTB8NVQSMVW2dWQWJdS9JcY8Vij/hSmrwbhqVVQA1D3gm4oHB
83iSMZYIyTtMS9MEgIFLh/OTiO2rsbxjDjjGIWKlqkECgYEA7E+zOOwb7jNdO9V+
Pa+wJ9qYqF6R+RnxoejH3oycHcs+ua+bkF6Fx4/fHtEqE2wZ+meTYtASkpcozspX
tBZzqVLG+2IBqjm81GFx+j7Kmini6TkKGtbSoppVhbB8aUpwshA1UAUd4Mo4DfrF
UnxCFeXejhWFjGZYWvJ+9+4+OVkCgYEA6YYvLmqGZYLuCYMDbDDj7Ru9S4n9IUzx
iGKEuBL700RmE5e3xOktOzSfypuDATG9NRkJqOq8XPesv2aPiSKe2UIs2aAjzjIh
G36JyuuKIpJ05wtsa8hnGq4W0/Asrfz7t3Sjps815J2t5VatS/DCIBsqRgix2ci6
PBTfBBCGpUkCgYAZxxPo2LCvSkEYXtCXkAuqPUk7zJ0lzDO0kr0CxNne0iX0gXh5
xiXXwBT5up1ZYwhubVABjmsIPJ8B98abaXuSc+oqz+UYmZXEoSCayLC3ImyTPlqg
1OBDRoyfucHIhotGkUgPg49fzw9QTxOpTM60rs+ZNjXMO/nVafB5wS5eeQKBgEoo
fpsy4PMjnQwQOuoErJ0Djh2K+IO2t40kHBmIjcgNlIzsRUOW+PIwR/5DXFrovLk7
9kFaKe8sCAPQQgy+nctIpI2HuVclvDXK/V8fZYKNF+q3SfsAbR2f40fyoX2vNRCR
TdcPPoIXQL3vxKC1GDCx777FzBTpFZs21Yuo49lhAoGAGTlZFrvJujKdy5jv11ZH
Ui93Br0py3SR6o0rxZlDgsI74WXVujTtsmSKNi+By+POK5LS4FrLzE2UvxAJlOfL
y2z3Cj+S4qgrxiJ8SgGvx+u9x713amL6JQwVPeUlYm3gLNhS0GPNnDZC62psJrFQ
JT/X3EFuU1U+61KJGIVUfTY=
-----END PRIVATE KEY-----
EOF

install -m 775 /tmp/tuic-server /opt/tuic-server
rm -f /tmp/tuic-server
/opt/tuic-server -c /opt/config.json
